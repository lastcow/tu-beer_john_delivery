package edu.towson.beer.services;

import edu.towson.beer.domain.RestaurantCategory;
import edu.towson.beer.domain.repository.RestaurantCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by beer_john on 7/4/17.
 */
public interface RestaurantCategoryService {

    /**
     * Save restaurant category
     * @param restaurantCategory
     * @return
     */
    public RestaurantCategory saveRestaurantCategory(RestaurantCategory restaurantCategory);

    /**
     * Get all restaurant categories
     * @return
     */
    public List<RestaurantCategory> getRestaurantCategories();
}
