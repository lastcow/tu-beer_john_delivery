package edu.towson.beer.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;

/**
 * Created by beer_john on 6/14/17.
 */

@Controller
public class HomeController {


    /**
     * Home page controller method
     * @param model
     * @return
     */
    @GetMapping("/")
    public String home(Map<String, Object> model){
        model.put("message", "Delivery Project");
        return "home";
    }


    /**
     * About page controller method
     * @param model
     * @return
     */
    @GetMapping("/about")
    public String about(Map<String, Object> model){
        return "about";
    }
}
