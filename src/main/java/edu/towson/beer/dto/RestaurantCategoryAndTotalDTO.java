package edu.towson.beer.dto;

/**
 * Created by beer_john on 7/4/17.
 */
public class RestaurantCategoryAndTotalDTO {

    private String categoryName;
    private int total;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
