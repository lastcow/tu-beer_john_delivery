
var subtotal = 0;
$(function(){

    var stripe = Stripe('pk_test_X8N4VRWXrqVFMWcjMbubjsqP');

    $( "a[name='btnAddToCart']" ).click(function (event){
        event.preventDefault();

        // Get form
        var form = $(this).closest('form');

        // Create an FormData object
        // var data = JSON.stringify(form.serializeArray());

        var postdata = new Object();
        var optionArray = [];
        var formData = form.serializeArray();

        _.each(formData, function(data){
            if(data.name === "menuOptionIds"){
                optionArray.push(data.value);
            }

            if(data.name === "menuId") {
                postdata.menuId = data.value;
            }
        });

        postdata.menuOptionIds = optionArray;


        $.ajax({
            type: "POST",
            url: form.attr('action'),
            contentType: "application/json",
            data: JSON.stringify(postdata),
            dataType: "json",
            cache: false,
            success: function(data){
                console.log(data);
                subtotal += data.price;
                $('#orderTotal').html(subtotal);
                $('#tblCart tbody').append('<tr><td><a href="#0" class="remove_item" onclick="javascript:removeItem(\'' +data.id+'\', $(this))"><i class="icon_minus_alt"></i></a> <strong>1x</strong> ' + data.name +
                    '</td><td><strong class="pull-right">'+ data.price +
                    '</strong></td></tr>');

            },
            error: function(e){
                console.log("ERROR: " + e);
            }
        });


        return false;
    });


    $("a[name=btnRemoveCartItem]").on('click', function(event){
        var aElement = $(this);
        var itemId = aElement.attr('itemid');

        removeItem(itemId, aElement);
    })

});

function removeItem(itemid, tdElement) {

    $.ajax({
        type: 'POST',
        url: '/order/removeitem',
        data: {id: itemid},
        success: function(data){
            if(data){
                // remove the item.
                tdElement.closest('tr').remove();

                $('#orderTotal').html(data.subtotal);
            }
        }
    })
}