
$(function(){

    new CBPFWTabs(document.getElementById('tabs'));
});


/**
 * Switch customer's status between enable/disable
 * @param id
 * @param status
 */
function switchCustomerStatus(id, status){

    var postdata = new Object();
    postdata.userid = id;
    postdata.active = status;

    $.ajax({
        type: "POST",
        url: "/user/updateStatus",
        contentType: "application/json",
        data: JSON.stringify(postdata),
        dataType: 'json',
        cache: false,
        success: function (data) {
            console.log(data);
        }
    });
}