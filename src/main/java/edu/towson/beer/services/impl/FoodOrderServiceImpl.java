package edu.towson.beer.services.impl;

import edu.towson.beer.domain.FoodOrder;
import edu.towson.beer.domain.FoodOrderHasItems;
import edu.towson.beer.domain.repository.FoodOrderItemRepository;
import edu.towson.beer.domain.repository.FoodOrderRepository;
import edu.towson.beer.services.FoodOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FoodOrderServiceImpl implements FoodOrderService {

    @Autowired
    FoodOrderRepository foodOrderRepository;
    @Autowired
    FoodOrderItemRepository foodOrderItemRepository;

    @Override
    public FoodOrder save(FoodOrder foodOrder) {
        return foodOrderRepository.save(foodOrder);
    }

    @Override
    public FoodOrderHasItems save(FoodOrderHasItems foodOrderHasItems) {
        return foodOrderItemRepository.save(foodOrderHasItems);
    }

    @Override
    public List<FoodOrder> listAllFoodOrders() {
        return foodOrderRepository.findAll();
    }


}
