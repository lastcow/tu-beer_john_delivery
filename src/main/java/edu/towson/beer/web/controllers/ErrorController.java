package edu.towson.beer.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by beer_john on 7/1/17.
 */
@Controller
public class ErrorController {

    /**
     * 403 Error
     * @return
     */
    @GetMapping("/403")
    public String error403(){
        return "/error/403";
    }
}
