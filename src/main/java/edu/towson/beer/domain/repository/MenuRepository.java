package edu.towson.beer.domain.repository;

import edu.towson.beer.domain.Menu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by beer_john on 7/5/17.
 */
@Repository
public interface MenuRepository extends JpaRepository<Menu, String> {
}
