package edu.towson.beer.form;

import java.util.List;

public class AddItemToCart {

    private String menuId;
    private List<String> menuOptionIds;

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public List<String> getMenuOptionIds() {
        return menuOptionIds;
    }

    public void setMenuOptionIds(List<String> menuOptionIds) {
        this.menuOptionIds = menuOptionIds;
    }
}
