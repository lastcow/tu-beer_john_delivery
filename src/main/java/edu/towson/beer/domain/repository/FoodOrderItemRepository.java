package edu.towson.beer.domain.repository;

import edu.towson.beer.domain.FoodOrderHasItems;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FoodOrderItemRepository extends JpaRepository<FoodOrderHasItems, String>{
}
