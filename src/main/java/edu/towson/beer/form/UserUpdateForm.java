package edu.towson.beer.form;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserUpdateForm {

    @Size(min = 8, message = "*Password length must >= 8")
    @NotNull(message = "*Password can't be null")
    @NotEmpty(message = "*Password can't be empty")
    private String password;
    private String passwordAgain;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordAgain() {
        return passwordAgain;
    }

    public void setPasswordAgain(String passwordAgain) {
        this.passwordAgain = passwordAgain;
    }
}
