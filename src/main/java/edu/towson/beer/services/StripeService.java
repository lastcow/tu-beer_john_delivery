package edu.towson.beer.services;

import com.stripe.exception.*;
import com.stripe.model.Charge;
import com.stripe.net.RequestOptions;
import edu.towson.beer.dto.StripeCharge;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class StripeService {

    @Value("${STRIPE_SECRET_KEY}")
    private String secureKey;

    public Charge charge(StripeCharge stripeCharge) throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
        RequestOptions requestOptions = (new RequestOptions.RequestOptionsBuilder()).setApiKey(secureKey).build();

        Map<String, Object> chargeMap = new HashMap<String, Object>();
        chargeMap.put("amount", stripeCharge.getAmount());
        chargeMap.put("currency", stripeCharge.getCurrency());
        chargeMap.put("source", stripeCharge.getToken()); // obtained via Stripe.js

        Charge charge = Charge.create(chargeMap, requestOptions);

        // return the result
        return charge;
    }
}
