

var stripe;
var card;
$(function(){

    stripe = Stripe('pk_test_wbM8TLEJFG3IrNOJ0RCctlwA');
    var elements = stripe.elements();
    card = elements.create('card');
    card.mount('#card-element');

});

/**
 * Submit charge.
 */
function submitCharge(){

    var promise = stripe.createToken(card);
    promise.then(function (result) {
        // Get token
        $('#token').val(result.token.id);
        // Submit form
        $('#formCheckout').submit();
    });

}