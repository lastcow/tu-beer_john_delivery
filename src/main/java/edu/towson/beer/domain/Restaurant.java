package edu.towson.beer.domain;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Required;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * Created by beer_john on 6/21/17.
 */
@Entity
public class Restaurant implements Serializable {


    @Id @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @NotEmpty
    private String name;
    private String tel;

    @Email(message = "*Please provide a valid Email")
    private String email;
    private String address;
    private boolean takeAway;
    private boolean delivery;
    private String openTime;
    private float minimalOrder;

    @OneToMany(mappedBy = "restaurant", fetch = FetchType.LAZY)
    private List<Menu> menuItemList;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "restaurant_category_id", nullable = false)
    private RestaurantCategory restaurantCategory;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isTakeAway() {
        return takeAway;
    }

    public void setTakeAway(boolean takeAway) {
        this.takeAway = takeAway;
    }

    public boolean isDelivery() {
        return delivery;
    }

    public void setDelivery(boolean delivery) {
        this.delivery = delivery;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public float getMinimalOrder() {
        return minimalOrder;
    }

    public void setMinimalOrder(float minimalOrder) {
        this.minimalOrder = minimalOrder;
    }

    public List<Menu> getMenuItemList() {
        return menuItemList;
    }

    public void setMenuItemList(List<Menu> menuItemList) {
        this.menuItemList = menuItemList;
    }

    public RestaurantCategory getRestaurantCategory() {
        return restaurantCategory;
    }

    public void setRestaurantCategory(RestaurantCategory restaurantCategory) {
        this.restaurantCategory = restaurantCategory;
    }
}
