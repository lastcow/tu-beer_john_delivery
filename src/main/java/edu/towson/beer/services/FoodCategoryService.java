package edu.towson.beer.services;

import edu.towson.beer.domain.FoodCategory;
import edu.towson.beer.domain.repository.FoodCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by beer_john on 7/5/17.
 */
public interface FoodCategoryService {

    public FoodCategory saveFoodCategory(FoodCategory foodCategory);
    public List<FoodCategory> getFoodCategories();

}
