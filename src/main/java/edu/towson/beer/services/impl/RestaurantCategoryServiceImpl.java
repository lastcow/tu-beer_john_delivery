package edu.towson.beer.services.impl;

import edu.towson.beer.domain.RestaurantCategory;
import edu.towson.beer.domain.repository.RestaurantCategoryRepository;
import edu.towson.beer.services.RestaurantCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service (value = "abc")
public class RestaurantCategoryServiceImpl implements RestaurantCategoryService {

    @Autowired
    RestaurantCategoryRepository restaurantCategoryRepository;

    /**
     * Create new restaurant category
     * @param restaurantCategory
     * @return
     */
    @Override
    public RestaurantCategory saveRestaurantCategory(RestaurantCategory restaurantCategory) {
        return restaurantCategoryRepository.save(restaurantCategory);
    }

    /**
     * Get list of categories
     * @return
     */
    @Override
    public List<RestaurantCategory> getRestaurantCategories() {
        return restaurantCategoryRepository.findAll(new Sort("name"));
    }
}
