package edu.towson.beer.services;

import edu.towson.beer.domain.CartItem;
import edu.towson.beer.domain.repository.CartItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CartItemService {

    @Autowired
    CartItemRepository cartItemRepository;

    /**
     * Create CartItem
     * @param cartItem
     * @return
     */
    public CartItem newCartItem(CartItem cartItem){
        return cartItemRepository.save(cartItem);
    }

    /**
     * Get Cart item list
     * @return
     */
    public List<CartItem> getAllCartItems(){
        return cartItemRepository.findAll();
    }

    public CartItem findByid(String id){
        return cartItemRepository.findOne(id);
    }

    public void deleteCartItem(CartItem cartItem){
        cartItemRepository.delete(cartItem);
    }
}
