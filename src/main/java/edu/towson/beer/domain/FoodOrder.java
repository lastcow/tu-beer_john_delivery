package edu.towson.beer.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by beer_john on 6/21/17.
 */
@Entity
public class FoodOrder implements Serializable {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    private boolean deliveried;
    private String status;
    private String address;             // User may required to different address

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_user_id", nullable = false)
    private User orderUser;

    @OneToMany(mappedBy = "foodOrder", fetch = FetchType.LAZY)
    private List<FoodOrderHasItems> foodOrderHasItems;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isDeliveried() {
        return deliveried;
    }

    public void setDeliveried(boolean deliveried) {
        this.deliveried = deliveried;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public User getOrderUser() {
        return orderUser;
    }

    public void setOrderUser(User orderUser) {
        this.orderUser = orderUser;
    }

    public List<FoodOrderHasItems> getFoodOrderHasItems() {
        return foodOrderHasItems;
    }

    public void setFoodOrderHasItems(List<FoodOrderHasItems> foodOrderHasItems) {
        this.foodOrderHasItems = foodOrderHasItems;
    }
}
