package edu.towson.beer.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class CartItem implements Serializable {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @ManyToOne
    @JoinColumn(name = "menu_id")
    private Menu menuItem;

    private int qty;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shoppingcart_id", nullable = false)
    private ShoppingCart shoppingCart;

    @ManyToMany
    @JoinTable(
            name = "cartitem_x_menuoption",
            joinColumns = @JoinColumn(name = "cartitem_id"),
            inverseJoinColumns = @JoinColumn(name = "menu_option_id")
    )
    private List<MenuOption> menuOptionList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Menu getMenuItem() {
        return menuItem;
    }

    public void setMenuItem(Menu menuItem) {
        this.menuItem = menuItem;
    }

    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

        public List<MenuOption> getMenuOptionList() {
        return menuOptionList;
    }

    public void setMenuOptionList(List<MenuOption> menuOptionList) {
        this.menuOptionList = menuOptionList;
    }
}
