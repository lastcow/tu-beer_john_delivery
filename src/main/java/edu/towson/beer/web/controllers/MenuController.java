package edu.towson.beer.web.controllers;

import edu.towson.beer.domain.*;
import edu.towson.beer.dto.ShoppingCartItem;
import edu.towson.beer.services.MenuService;
import edu.towson.beer.services.RestaurantService;
import edu.towson.beer.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by beer_john on 7/5/17.
 */
@Controller
public class MenuController {

    @Autowired
    MenuService menuService;
    @Autowired
    RestaurantService restaurantService;
    @Autowired
    UserService userService;

    @GetMapping(value = "/restaurant_menu")
    public String displayMenu(ModelMap modelMap,
                              @RequestParam("id") String restaurantId,
                              Principal principal){


        // Only for login user
        if(principal != null){
            // Get authorized user
            User user = userService.findUserByEmail(principal.getName());

            List<ShoppingCartItem> shoppingCartItems = new ArrayList<>();
            // Get shopping cart
            ShoppingCart shoppingCart = user.getShoppingCart();

            if(shoppingCart != null) {
                for (CartItem cartItem : shoppingCart.getCartItemList()) {
                    ShoppingCartItem shoppingCartItem = new ShoppingCartItem();
                    shoppingCartItem.setName(cartItem.getMenuItem().getName());
                    shoppingCartItem.setQty(1);
                    float subtotal = cartItem.getMenuItem().getPrice();
                    for (MenuOption menuOption : cartItem.getMenuOptionList()) {
                        subtotal += menuOption.getCost();
                    }
                    shoppingCartItem.setCost(subtotal);

                    shoppingCartItems.add(shoppingCartItem);
                }

                // bring to front
                modelMap.addAttribute("shoppingcart", shoppingCartItems);
            }
        }

        // Get restaurant information
        Restaurant restaurant = restaurantService.getRestaurantById(restaurantId);
        modelMap.addAttribute("restaurant", restaurant);

        // Get restaurant menu details

        return "restaurant_menu";
    }
}
