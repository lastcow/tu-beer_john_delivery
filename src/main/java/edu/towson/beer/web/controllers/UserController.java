package edu.towson.beer.web.controllers;

import edu.towson.beer.domain.FoodOrder;
import edu.towson.beer.domain.Role;
import edu.towson.beer.domain.User;
import edu.towson.beer.form.UserForm;
import edu.towson.beer.form.UserUpdateForm;
import edu.towson.beer.services.RoleService;
import edu.towson.beer.services.UserService;
import org.hibernate.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by beer_john on 6/26/17.
 */

@Controller
public class UserController {

    @Autowired
    UserService userService;
    @Autowired
    RoleService roleService;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * User login page
     * @param model
     * @return
     */
    @GetMapping(value = "/login")
    public String login(Map<String, Object> model){

        return "login";
    }

    @GetMapping(value = "/register")
    public String register(Model model){

        model.addAttribute("user", new User());
        return "register";
    }

    @PostMapping(value = "/register")
    public String register(@Valid User user, BindingResult bindingResult){

        User userExists = userService.findUserByEmail(user.getEmail());

        if(userExists != null){
            bindingResult.rejectValue("email", "error.user", "User existing");
            return "register";
        }
        if(!user.getPassword().equals(user.getPasswordAgain())){
            bindingResult.rejectValue("passwordAgain", "passwords.not.match", "Password not match !");
        }
        if(bindingResult.hasErrors()){
            return "register";
        }
        else{
            Role userRole = roleService.findByRole("ROLE_USER");
            user.setActive(true);
            user.setRole(userRole);
            userService.saveUser(user);
        }
        return "home";
    }

    /**
     * Update user's status
     * @param model
     * @param userform
     * @return
     */
    @PostMapping(value = "/user/updateStatus", produces = "application/json")
    @ResponseBody
    public User switchUserStatus(Model model,
                                    @RequestBody UserForm userform){

        try {
            // Get user based on id.
            User user = userService.findUserById(userform.getUserid());
            user.setActive(userform.isActive());

            userService.saveUser(user);

            return user;
        }catch (Exception ex){
            return null;
        }
    }

    /***
     * Display my orders
     * @param modelMap
     * @param principal
     * @return
     */
    @GetMapping(value = "/user/admin")
    public String showTracking(ModelMap modelMap,
                               Principal principal){

        User user = userService.findUserByEmail(principal.getName());
        List<FoodOrder> foodOrderList = user.getFoodOrderList();
        UserUpdateForm userUpdateForm = new UserUpdateForm();

        modelMap.addAttribute("foodOrders", foodOrderList);
        modelMap.addAttribute("user", userUpdateForm);
        return "admin_user";
    }


    @PostMapping(value = "/user/update")
    public String updatePassword(ModelMap modelMap,
                                 @ModelAttribute("user") @Valid UserUpdateForm userUpdateForm,
                                 BindingResult bindingResult,
                                 Principal principal){

        User user = userService.findUserByEmail(principal.getName());
        List<FoodOrder> foodOrderList = user.getFoodOrderList();
        modelMap.addAttribute("foodOrders", foodOrderList);
        modelMap.addAttribute("user", userUpdateForm);
        // Check for password
        if(!userUpdateForm.getPassword().equals(userUpdateForm.getPasswordAgain())){
            bindingResult.rejectValue("passwordAgain", "passwords.not.match", "Password not match !");
        }

        if(bindingResult.hasErrors()){

        }else{
            // Update user's password
            user.setPassword(userUpdateForm.getPassword());
            userService.saveUser(user);
        }


        return "admin_user";
    }
}
