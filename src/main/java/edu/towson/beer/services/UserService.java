package edu.towson.beer.services;

import edu.towson.beer.domain.User;

import java.util.List;

/**
 * Created by beer_john on 6/21/17.
 */
public interface UserService {

    public User findUserById(String id);
    public User findUserByEmail(String email);
    public User saveUser(User user);
    public List<User> findUserByRole(String role);
}
