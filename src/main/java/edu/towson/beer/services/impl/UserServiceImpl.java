package edu.towson.beer.services.impl;


import edu.towson.beer.domain.Role;
import edu.towson.beer.domain.User;
import edu.towson.beer.domain.repository.RoleRepository;
import edu.towson.beer.domain.repository.UserRepository;
import edu.towson.beer.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    @Override
    public User findUserById(String id) {
        return userRepository.findOne(id);
    }

    @Override
    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public User saveUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    /**
     * List user by role name
     * @param role
     * @return
     */
    public List<User> findUserByRole(String role){
        return userRepository.findByRole_Role(role);
    }
}
