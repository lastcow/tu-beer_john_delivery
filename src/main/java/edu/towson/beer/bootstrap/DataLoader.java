package edu.towson.beer.bootstrap;

import com.sun.org.apache.regexp.internal.RE;
import edu.towson.beer.domain.*;
import edu.towson.beer.domain.repository.RoleRepository;
import edu.towson.beer.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * Created by beer_john on 6/21/17.
 */
@Component
public class DataLoader implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    UserService userService;
    @Autowired
    RestaurantCategoryService restaurantCategoryService;
    @Autowired
    RestaurantService restaurantService;
    @Autowired
    MenuService menuService;
    @Autowired
    FoodCategoryService foodCategoryService;
    @Autowired
    MenuOptionService menuOptionService;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    RoleService roleService;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        Role role = new Role();
        role.setRole("ROLE_USER");
        roleService.saveRole(role);


        User user = new User();
        user.setFirstName("Beer");
        user.setLastName("John");
        user.setEmail("beer_john@gmail.com");
        user.setContactNumber("1234567890");
        user.setActive(true);
        user.setPassword("abc");
        user.setRole(role);
//        user.setPassword("abc");
        userService.saveUser(user);

        role = new Role();
        role.setRole("ROLE_ADMIN");
        roleService.saveRole(role);

        user = new User();
        user.setFirstName("Beer");
        user.setLastName("John");
        user.setEmail("jbe1984@gmail.com");
        user.setContactNumber("3019993929");
        user.setActive(true);
        user.setPassword("abc");
        user.setRole(role);
        userService.saveUser(user);


        // Add default Restaurant category
        RestaurantCategory restaurantCategory = new RestaurantCategory();
        restaurantCategory.setName("Mexican / American");
        restaurantCategoryService.saveRestaurantCategory(restaurantCategory);

        Restaurant restaurant = new Restaurant();
        restaurant.setRestaurantCategory(restaurantCategory);
        restaurant.setName("Taco Mexican");
        restaurant.setAddress("135 Newtownards Road, Belfast, BT4");
        restaurant.setDelivery(false);
        restaurant.setOpenTime("Monday to Friday 9.00am - 7.30pm");
        restaurant.setTakeAway(true);
        restaurant.setMinimalOrder(5f);
        restaurant.setTel("123-456-798");
        restaurantService.saveRestaurant(restaurant);

        restaurant = new Restaurant();
        restaurant.setRestaurantCategory(restaurantCategory);
        restaurant.setName("Taco Mexican - 1");
        restaurant.setAddress("135 Newtownards Road, Belfast, BT4");
        restaurant.setDelivery(false);
        restaurant.setOpenTime("17:00");
        restaurant.setTakeAway(true);
        restaurant.setMinimalOrder(5f);
        restaurant.setTel("123-456-998");
        restaurantService.saveRestaurant(restaurant);

        FoodCategory foodCategory = new FoodCategory();
        foodCategory.setName("Soup");
        foodCategory.setDesc("Soup");
        foodCategoryService.saveFoodCategory(foodCategory);

        Menu menu = new Menu();
        menu.setName("Soup");
        menu.setRestaurant(restaurant);
        menu.setPrice(8.99f);
        menu.setDescription("Soup test menu item");
        menu.setFoodCategory(foodCategory);
        menuService.saveMenu(menu);

        MenuOption menuOption = new MenuOption();
        menuOption.setCost(3.44f);
        menuOption.setName("Special");
        menuOption.setDescription("Special option");
        menuOption.setMenuItem(menu);
        menuOptionService.saveMenuOption(menuOption);

        menuOption = new MenuOption();
        menuOption.setCost(4.99f);
        menuOption.setName("Special Optino 2");
        menuOption.setDescription("Special option");
        menuOption.setMenuItem(menu);
        menuOptionService.saveMenuOption(menuOption);

        menu = new Menu();
        menu.setName("Fish");
        menu.setRestaurant(restaurant);
        menu.setPrice(29.99f);
        menu.setDescription("Soup test menu item");
        menu.setFoodCategory(foodCategory);
        menuService.saveMenu(menu);





        restaurantCategory = new RestaurantCategory();
        restaurantCategory.setName("Italian / Pizza");
        restaurantCategoryService.saveRestaurantCategory(restaurantCategory);

        restaurantCategory = new RestaurantCategory();
        restaurantCategory.setName("Sushi / Japanese");
        restaurantCategoryService.saveRestaurantCategory(restaurantCategory);

        restaurantCategory = new RestaurantCategory();
        restaurantCategory.setName("Chinese / Thai");
        restaurantCategoryService.saveRestaurantCategory(restaurantCategory);

        restaurantCategory = new RestaurantCategory();
        restaurantCategory.setName("Chinese / Vietnam");
        restaurantCategoryService.saveRestaurantCategory(restaurantCategory);

    }
}
