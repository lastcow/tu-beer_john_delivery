package edu.towson.beer.domain.repository;

import edu.towson.beer.domain.FoodOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by beer_john on 6/21/17.
 */
@Repository
public interface FoodOrderRepository extends JpaRepository<FoodOrder, String> {
}
