package edu.towson.beer.domain.repository;


import edu.towson.beer.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, String> {

//    @Query("SELECT role FROM Role role WHERE role.role = ?1")
    Role findByRole(String role);

}
