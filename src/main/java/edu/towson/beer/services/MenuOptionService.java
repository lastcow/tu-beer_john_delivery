package edu.towson.beer.services;

import edu.towson.beer.domain.MenuOption;
import edu.towson.beer.domain.repository.MenuOptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

public interface MenuOptionService {


    public MenuOption findById(String id);

    /**
     * Save menu option
     * @param menuOption
     * @return
     */
    public MenuOption saveMenuOption(MenuOption menuOption);
}
