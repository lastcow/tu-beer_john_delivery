package edu.towson.beer.services.impl;

import edu.towson.beer.domain.FoodCategory;
import edu.towson.beer.domain.repository.FoodCategoryRepository;
import edu.towson.beer.services.FoodCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FoodCategoryServiceImpl implements FoodCategoryService{

    @Autowired
    FoodCategoryRepository foodCategoryRepository;

    /**
     * Save food category
     * @param foodCategory
     * @return
     */
    @Override
    public FoodCategory saveFoodCategory(FoodCategory foodCategory) {
        return foodCategoryRepository.save(foodCategory);
    }

    /**
     * Get all food categories
     * @return
     */
    @Override
    public List<FoodCategory> getFoodCategories() {
        return foodCategoryRepository.findAll(new Sort("name"));
    }
}
