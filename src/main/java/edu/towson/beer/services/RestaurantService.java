package edu.towson.beer.services;

import edu.towson.beer.domain.Restaurant;
import edu.towson.beer.domain.repository.RestaurantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by beer_john on 6/21/17.
 */
@Service
public class RestaurantService {

    @Autowired
    RestaurantRepository restaurantRepository;

    /**
     * New Restaurant
     * @param restaurant
     * @return
     */
    public Restaurant saveRestaurant (Restaurant restaurant){
        return restaurantRepository.save(restaurant);
    }


    /**
     * Get list of restaurants
     * @return
     */
    public List<Restaurant> getRestaurants(){
        return restaurantRepository.findAll();
    }

    /**
     * Get restaurant by id.
     * @param id
     * @return
     */
    public Restaurant getRestaurantById(String id){
        return restaurantRepository.findOne(id);
    }
}
