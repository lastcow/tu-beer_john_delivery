package edu.towson.beer.domain.repository;

import edu.towson.beer.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by beer_john on 6/21/17.
 */
@Repository
public interface UserRepository extends JpaRepository<User, String> {

//    @Query("SELECT user FROM User user WHERE user.email = ?1")
    User findByEmail(String email);

    /**
     * Find user by role
     * @param role
     * @return
     */
    List<User> findByRole_Role(String role);
}
