package edu.towson.beer.services;

import edu.towson.beer.domain.Menu;
import edu.towson.beer.domain.repository.MenuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by beer_john on 7/5/17.
 */
@Service
public class MenuService {

    @Autowired
    MenuRepository menuRepository;

    /**
     * Save menu
     * @param menu
     * @return
     */
    public Menu saveMenu(Menu menu){
        return menuRepository.save(menu);
    }

    /**
     * Find menu by id
     * @param id
     * @return
     */
    public Menu findMenuById(String id) {
        return  menuRepository.findOne(id);
    }
}
