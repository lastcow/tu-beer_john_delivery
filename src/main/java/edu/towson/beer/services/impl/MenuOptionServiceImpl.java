package edu.towson.beer.services.impl;

import edu.towson.beer.domain.MenuOption;
import edu.towson.beer.domain.repository.MenuOptionRepository;
import edu.towson.beer.services.MenuOptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class MenuOptionServiceImpl implements MenuOptionService {

    @Autowired
    MenuOptionRepository menuOptionRepository;


    @Override
    public MenuOption findById(String id){
        return menuOptionRepository.findOne(id);
    }

    @Override
    public MenuOption saveMenuOption(MenuOption menuOption) {
        return menuOptionRepository.save(menuOption);
    }
}
