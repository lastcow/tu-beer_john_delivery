package edu.towson.beer.web.controllers;


import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.stripe.exception.*;
import com.stripe.model.Charge;
import edu.towson.beer.domain.*;
import edu.towson.beer.dto.CreditCard;
import edu.towson.beer.dto.DeliveryAddress;
import edu.towson.beer.dto.ShoppingCartItem;
import edu.towson.beer.dto.StripeCharge;
import edu.towson.beer.form.AddItemToCart;
import edu.towson.beer.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
public class OrderController {

    @Autowired
    MenuService menuService;

    @Autowired
    ShoppingCartService shoppingCartService;

    @Autowired
    CartItemService cartItemService;

    @Autowired
    UserService userService;

    @Autowired
    MenuOptionService menuOptionService;

    @Autowired
    StripeService stripeService;

    @Autowired
    FoodOrderService foodOrderService;

    /**
     * Add to user's shopping cart
     * @param addItemToCart
     * @param principal
     * @return
     */
    @PostMapping(value = "/order/addtocart", produces = "application/json")
    @ResponseBody
    public String addToShoppingCart(@RequestBody AddItemToCart addItemToCart,
                                    Principal principal){

        float subtotal = 0;

        // Get current user
        User user = userService.findUserByEmail(principal.getName());

        ShoppingCart shoppingCart = user.getShoppingCart();

        // Check for shopping cart
        if(shoppingCart == null){
            shoppingCart = new ShoppingCart();
            shoppingCart.setUser(user);
            shoppingCartService.createShoppingCart(shoppingCart);
        }

        List<MenuOption> menuOptions = new ArrayList<>();
        // Get list of manuoptions
        for(String moId : addItemToCart.getMenuOptionIds()){
            MenuOption menuOption = menuOptionService.findById(moId);
            menuOptions.add(menuOption);
            subtotal += menuOption.getCost();
        }

        Menu menu = menuService.findMenuById(addItemToCart.getMenuId());
        CartItem cartItem = new CartItem();
        cartItem.setMenuItem(menu);
        cartItem.setShoppingCart(shoppingCart);
        cartItem.setMenuOptionList(menuOptions);
        cartItem = cartItemService.newCartItem(cartItem);

        JsonObject jsonObject = new JsonObject();
        jsonObject.add("id", new JsonPrimitive(cartItem.getId()));
        jsonObject.add("name", new JsonPrimitive(cartItem.getMenuItem().getName()));
        jsonObject.add("price", new JsonPrimitive(subtotal + cartItem.getMenuItem().getPrice()));

        return (new Gson()).toJson(jsonObject);
    }

    @PostMapping(value = "/order/removeitem", produces = "application/json")
    @ResponseBody
    public String removeFromShoppingCart(@PathParam("id") String id,
                                         Principal principal){

        // Get Cart item
        CartItem cartItem = cartItemService.findByid(id);
        cartItemService.deleteCartItem(cartItem);

        List<ShoppingCartItem> shoppingCartItems = setShoppingCart(principal);
        float orderTotal = getOrderTotal(shoppingCartItems);

        JsonObject jsonObject = new JsonObject();
        jsonObject.add("subtotal", new JsonPrimitive(orderTotal));

        return (new Gson()).toJson(jsonObject);
    }


    @GetMapping(value = "/order/cart1")
    public String orderStep1(ModelMap modelMap,
                             Principal principal){


        List<ShoppingCartItem> shoppingCartItems = setShoppingCart(principal);
        modelMap.addAttribute("shoppingcart", shoppingCartItems);
        float orderTotal = getOrderTotal(shoppingCartItems);

        // The address object
        DeliveryAddress deliveryAddress = new DeliveryAddress();
        modelMap.addAttribute("deliveryAddress", deliveryAddress);
        modelMap.addAttribute("orderTotal", orderTotal);
        return "cart";
    }


    @PostMapping(value = "/order/cart2")
    public String orderStep2(ModelMap modelMap,
                             Principal principal,
                             @Valid DeliveryAddress deliveryAddress,
                             BindingResult bindingResult){

        if(bindingResult.hasErrors()){
            modelMap.addAttribute("shoppingcart", setShoppingCart(principal));
            return "cart";
        }


        List<ShoppingCartItem> shoppingCartItems = setShoppingCart(principal);
        float orderTotal = getOrderTotal(shoppingCartItems);
        StripeCharge stripeCharge = new StripeCharge();
        modelMap.addAttribute("sc", stripeCharge);
        modelMap.addAttribute("deliveryAddress", deliveryAddress);
        modelMap.addAttribute("orderTotal", orderTotal);
        modelMap.addAttribute("shoppingcart", shoppingCartItems);
        return "checkout";
    }

    @PostMapping(value = "/order/checkout")
    public String orderCheckOut(ModelMap modelMap,
                                @Valid StripeCharge sc,
                                @Valid DeliveryAddress deliveryAddress,
                                BindingResult bindingResult,
                                Principal principal){

        if(bindingResult.hasErrors()){

            return "cart";
        }

        User user = userService.findUserByEmail(principal.getName());
        // Get user's shopping cart.
        List<ShoppingCartItem> shoppingCartItems = setShoppingCart(principal);
        float orderTotal = getOrderTotal(shoppingCartItems);
        // Stripe process
        // Get shopping cart
        ShoppingCart shoppingCart = user.getShoppingCart();
        sc.setAmount((int) (orderTotal*100));
        sc.setCurrency("usd");
        try {
            Charge charge = stripeService.charge(sc);
            if(charge.getPaid()){

                // Save to order
                FoodOrder foodOrder = new FoodOrder();
                foodOrder.setAddress(deliveryAddress.getAddress());
                foodOrder.setDeliveried(false);
                foodOrder.setStatus("ordered");
                foodOrder.setOrderUser(user);
                foodOrderService.save(foodOrder);

                List<FoodOrderHasItems> foodOrderHasItems = new ArrayList<>();
                for(CartItem cartItem : shoppingCart.getCartItemList()){
                    FoodOrderHasItems item = new FoodOrderHasItems();
                    item.setMenuItem(cartItem.getMenuItem());
                    item.setQuantity(cartItem.getQty());
                    List<MenuOption> menuOptions = new ArrayList<>();
                    for(MenuOption mo : cartItem.getMenuOptionList()){
                        menuOptions.add(mo);
                    }
                    item.setMenuOptionList(menuOptions);
                    item.setFoodOrder(foodOrder);
                    foodOrderHasItems.add(item);
                    foodOrderService.save(item);
                }
//                foodOrder.setFoodOrderHasItems(foodOrderHasItems);


                modelMap.addAttribute("items", shoppingCartItems);
                modelMap.addAttribute("orderTotal", orderTotal);
                return "orderCompleted";
            }
        } catch (CardException e) {
            e.printStackTrace();
        } catch (APIException e) {
            e.printStackTrace();
        } catch (AuthenticationException e) {
            e.printStackTrace();
        } catch (InvalidRequestException e) {
            e.printStackTrace();
        } catch (APIConnectionException e) {
            e.printStackTrace();
        }
        return "cart";
    }

    /**
     * Get order total...
     * @param shoppingCartItems
     * @return
     */
    private float getOrderTotal(List<ShoppingCartItem> shoppingCartItems){

        float orderTotal = 0;
        for(ShoppingCartItem shoppingCartItem : shoppingCartItems){
            orderTotal += shoppingCartItem.getCost();
        }

        return orderTotal;
    }

    /**
     * add shopping cart to ModelMap
     * @param principal
     */
    private List<ShoppingCartItem> setShoppingCart(Principal principal){

        List<ShoppingCartItem> shoppingCartItems = new ArrayList<>();
        // Only for login user
        if(principal != null){
            // Get authorized user
            User user = userService.findUserByEmail(principal.getName());

            // Get shopping cart
            ShoppingCart shoppingCart = user.getShoppingCart();

            if(shoppingCart != null) {
                for (CartItem cartItem : shoppingCart.getCartItemList()) {
                    ShoppingCartItem shoppingCartItem = new ShoppingCartItem();
                    shoppingCartItem.setName(cartItem.getMenuItem().getName());
                    shoppingCartItem.setQty(1);
                    float subtotal = cartItem.getMenuItem().getPrice();
                    for (MenuOption menuOption : cartItem.getMenuOptionList()) {
                        subtotal += menuOption.getCost();
                    }
                    shoppingCartItem.setCost(subtotal);
                    shoppingCartItem.setId(cartItem.getId());

                    shoppingCartItems.add(shoppingCartItem);
                }

                // bring to front
                return shoppingCartItems;
            }
        }

        return shoppingCartItems;
    }
}
