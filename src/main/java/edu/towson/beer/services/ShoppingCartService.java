package edu.towson.beer.services;

import edu.towson.beer.domain.ShoppingCart;
import edu.towson.beer.domain.repository.ShoppingCartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ShoppingCartService  {

    @Autowired
    ShoppingCartRepository shoppingCartRepository;

    /**
     * Save shopping cart
     * @param shoppingCart
     * @return
     */
    public ShoppingCart createShoppingCart(ShoppingCart shoppingCart){
        return shoppingCartRepository.save(shoppingCart);
    }

    /**
     * Get first shopping cart
     * @return
     */
    public ShoppingCart getFirstShoppingCart()
    {
        List<ShoppingCart> shoppingCarts = shoppingCartRepository.findAll();
        if(shoppingCarts.size() > 0){
            return shoppingCarts.get(0);
        }else{
            return null;
        }
    }
}
