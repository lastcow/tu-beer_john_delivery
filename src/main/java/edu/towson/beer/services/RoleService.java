package edu.towson.beer.services;

import edu.towson.beer.domain.Role;

public interface RoleService {

    public Role saveRole(Role role);
    public Role findByRole(String role);
}
