package edu.towson.beer.services.impl;

import edu.towson.beer.domain.Role;
import edu.towson.beer.domain.repository.RoleRepository;
import edu.towson.beer.services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    RoleRepository roleRepository;

    @Override
    public Role saveRole(Role role) {
        return roleRepository.save(role);
    }

    @Override
    public Role findByRole(String role) {
        return roleRepository.findByRole(role);
    }


}
