package edu.towson.beer.web.controllers;


import edu.towson.beer.domain.FoodOrder;
import edu.towson.beer.domain.Restaurant;
import edu.towson.beer.domain.User;
import edu.towson.beer.services.FoodOrderService;
import edu.towson.beer.services.RestaurantService;
import edu.towson.beer.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class AdminController {

    @Autowired
    UserService userService;

    @Autowired
    RestaurantService restaurantService;

    @Autowired
    FoodOrderService foodOrderService;

    @GetMapping(value = "/admin")
    public String showAdminPanel(Model model){

        // Get restaurants
        List<Restaurant> restaurants = restaurantService.getRestaurants();
        model.addAttribute("restaurants", restaurants);

        // Get customers
        List<User> customers = userService.findUserByRole("ROLE_USER");
        model.addAttribute("customers", customers);

        // Get orders
        List<FoodOrder> foodOrders = foodOrderService.listAllFoodOrders();
        model.addAttribute("foodOrders", foodOrders);

        return "admin";
    }
}
