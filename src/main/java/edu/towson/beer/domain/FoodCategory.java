package edu.towson.beer.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by beer_john on 6/21/17.
 */
@Entity
public class FoodCategory implements Serializable {

    @Id @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;
    private String name;

//    @OneToMany(fetch = FetchType.LAZY, mappedBy = "foodCategory")
//    private List<Menu> menuItems;

    @Column(name = "description")
    private String desc;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

//    public List<Menu> getMenuItems() {
//        return menuItems;
//    }
//
//    public void setMenuItems(List<Menu> menuItems) {
//        this.menuItems = menuItems;
//    }
}
