package edu.towson.beer.domain.repository;

import edu.towson.beer.domain.FoodCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by beer_john on 7/4/17.
 */
@Repository
public interface FoodCategoryRepository extends JpaRepository<FoodCategory, String> {
}
