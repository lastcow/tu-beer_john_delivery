package edu.towson.beer.services;

import edu.towson.beer.domain.FoodOrder;
import edu.towson.beer.domain.FoodOrderHasItems;

import java.util.List;

public interface FoodOrderService {

    public FoodOrder save(FoodOrder foodOrder);
    public FoodOrderHasItems save(FoodOrderHasItems foodOrderHasItems);
    public List<FoodOrder> listAllFoodOrders();
}
