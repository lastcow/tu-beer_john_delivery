package edu.towson.beer.web.controllers;

import edu.towson.beer.domain.FoodCategory;
import edu.towson.beer.domain.Restaurant;
import edu.towson.beer.domain.RestaurantCategory;
import edu.towson.beer.dto.RestaurantCategoryAndTotalDTO;
import edu.towson.beer.services.RestaurantCategoryService;
import edu.towson.beer.services.RestaurantService;
import edu.towson.beer.services.impl.FoodCategoryServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by beer_john on 7/2/17.
 */
@Controller
public class RestaurantController {

    @Autowired
    RestaurantService restaurantService;

    @Autowired @Qualifier("abc")
    RestaurantCategoryService restaurantCategoryService;
    @Autowired
    FoodCategoryServiceImpl foodCategoryService;


    /**
     * New restaurant page
     * @param model
     * @return
     */
    @GetMapping(value = "/admin/restaurant/new")
    public String newRestaurantGet(ModelMap model){

        // Get Food categories
        List<RestaurantCategory> restaurantCategories = restaurantCategoryService.getRestaurantCategories();
        model.addAttribute("categories", restaurantCategories);

        // Init empty restaurant
        Restaurant restaurant = new Restaurant();
        model.addAttribute("restaurant", restaurant);
        return "newrestaurant";
    }

    @PostMapping(value = "/admin/restaurant/new")
    public String newRestaurantPost(@Valid Restaurant restaurant,
                                    BindingResult bindingResult,
                                    ModelMap modelMap){

        if(bindingResult.hasErrors()){

            // Get Food categories
            List<RestaurantCategory> restaurantCategories = restaurantCategoryService.getRestaurantCategories();
            modelMap.addAttribute("categories", restaurantCategories);

            return "newrestaurant";
        }
        restaurantService.saveRestaurant(restaurant);
        return "redirect:/admin";
    }
    /**
     * Display restaurant page
     * @param model
     * @return
     */
    @GetMapping("/restaurant")
    public String restaurantDefault(ModelMap model){

        // Get total Restaurant categories and it's total
        List<RestaurantCategoryAndTotalDTO> restaurantCategoryAndTotalDTOS = new ArrayList<>();
        List<RestaurantCategory> restaurantCategoryList = restaurantCategoryService.getRestaurantCategories();

        RestaurantCategoryAndTotalDTO restaurantCategoryAndTotalDTO = new RestaurantCategoryAndTotalDTO();
        restaurantCategoryAndTotalDTO.setCategoryName("All");
        restaurantCategoryAndTotalDTO.setTotal(restaurantService.getRestaurants().size());
        restaurantCategoryAndTotalDTOS.add(restaurantCategoryAndTotalDTO);

        for (RestaurantCategory restaurantCategory : restaurantCategoryList
             ) {
            restaurantCategoryAndTotalDTO = new RestaurantCategoryAndTotalDTO();
            restaurantCategoryAndTotalDTO.setCategoryName(restaurantCategory.getName());
            restaurantCategoryAndTotalDTO.setTotal(restaurantCategory.getRestaurantList().size());
            restaurantCategoryAndTotalDTOS.add(restaurantCategoryAndTotalDTO);
        }

        model.addAttribute("restaurantlist", restaurantService.getRestaurants());
        model.addAttribute("restaurantCategoriesList", restaurantCategoryAndTotalDTOS);
        return "restaurant";
    }
}
